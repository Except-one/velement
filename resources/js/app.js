
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */
import App from './App.vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueRouter from 'vue-router';

Vue.use(ElementUI);
Vue.use(VueRouter);

import Home from './components/Example.vue'
import Task from './components/Task.vue'
import Publish from './components/Publish.vue'
import Prome from './components/Promo.vue'
import Mine from './components/Mine.vue'
import Profile from './components/mine/Profile.vue'
import Withdraw from './components/mine/Withdraw.vue'
import Charge from './components/mine/Charge.vue'

import NotFound404 from './components/NotFound.vue'

let router = new VueRouter({
    mode: "history",
    routes: [
        { path: '', redirect: '/home' },
        { path: '/home', component: Home },
        { path: '/task', component: Task },
        { path: '/publish', component: Publish },
        { path: '/promo', component: Prome },
        { path: '/mine', component: Mine },
        { path: '/profile', component: Profile},
        { path: '/withdraw', component: Withdraw},
        { path: '/charge', component: Charge},
        { path: '*', redirect: '/home' },
    ]
})

const app = new Vue({
  el: '#app',
  router: router,
  render: h => h(App),
});
