<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Laravel + Vue + ElementUI</title>
</head>
<body>
  <div id="app"></div>

  <script src="{{ mix('js/app.js') }}?v={{ time() }}"></script>
</body>
</html>
