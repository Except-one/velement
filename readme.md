# Element In Laravel

[Laravel 5.7](https://learnku.com/docs/laravel/5.7/releases/2239) Project with [Vue2](https://cn.vuejs.org/v2/guide/) and [Element](https://github.com/ElemeFE/element)

## Usage
```
git clone https://gitee.com/handers/velement.git <project-name>
```
```
cd <project-name>
```
```
composer install
```
```
cp .env.example .env
```
```
php artisan key:generate
```
```
php artisan serve (if not using laravel valet)
```
```
yarn OR npm install
```
```
yarn watch OR npm run watch
```

Visit `http://localhost:8000`, It works!

```
If using laravel valet, visit http://<project-name>.test
```

Now you're ready to start coding!

## More About ElementUI

The articles can help you to deploy it by yourself:

- [Laravel5.7 + Vue2 + ElementUI](https://github.com/ElementUI/element-in-laravel-starter.git)
- [Try Laravel 5.3 + Vue2 with Element](http://codesky.me/archives/try-laravel5-vue2-element-en.wind)
- [Laravel 5.3 + Vue2 + Element试水](http://codesky.me/archives/try-laravel5-vue2-element-cn.wind)
- [Element Docs](https://element.eleme.io/#/en-US)